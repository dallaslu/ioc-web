package com.dallaslu.ioc.web;

import javax.servlet.ServletContext;

import com.dallaslu.ioc.BeanContext;

public class BeanUtils {
	
	private ServletContext servletContext ;
	
	private BeanUtils(){
		
	}
	private static class Holder{
		public static BeanUtils instance = new BeanUtils();
	}
	public static BeanUtils getInstance(){
		return Holder.instance;
	}
	
	public BeanContext getBeanContext(ServletContext sc){
		return (BeanContext) sc.getAttribute("beanContext");
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
}
