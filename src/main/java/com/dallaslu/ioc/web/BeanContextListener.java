package com.dallaslu.ioc.web;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dallaslu.ioc.BeanContext;
import com.dallaslu.ioc.BeanFactory;

public class BeanContextListener implements ServletContextListener {

	private static final Log log = LogFactory.getLog(BeanContextListener.class);

	public void contextDestroyed(ServletContextEvent sce) {

	}

	public void contextInitialized(ServletContextEvent sce) {
		String filePath = sce.getServletContext().getInitParameter("ioc-config");
		filePath = sce.getServletContext().getRealPath(filePath);
		log.debug("ioc configuration file: " + filePath);

		try {
			BeanContext bc = BeanFactory.getInstance().parse(filePath);
			sce.getServletContext().setAttribute("beanContext", bc);

		} catch (Exception e) {
			log.debug("error when initialize context: ", e);
		}

		BeanUtils.getInstance().setServletContext(sce.getServletContext());
	}

}
