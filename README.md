# ioc-web
web utils of [ioc](https://github.com/dallaslu/ioc)

# Maven
    <dependency>
      <groupId>com.dallaslu.ioc</groupId>
      <artifactId>web</artifactId>
      <version>0.0.1</version>
    </dependency>

# How to
1. add listenter `com.dallaslu.ioc.web.BeanContextListener` in web.xml;
2. add initparam `ioc-config` with configuration filepath as value ([example](https://github.com/dallaslu/ioc/blob/master/src/test/resources/application-context.xml));
3. use `BeanUtils.getInstance().getBeanContext(request.getServletContext()).getBean( beanId)` in servlet.
